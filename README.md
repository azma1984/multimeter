Requirements:
Qt 5.9.5+
GCC 7.3.0+

Build
MultimeterServer and MultimeterClient can be built with gcc. Build files are generated using qmake.
Start Qt Creator. 
Open file MultimeterClient/MultimeterClient/multimeterclient.pro and build release.
Open file MultimeterServer/MultimeterServer/multimeterserver.pro and build release.