#ifndef CLIENT_H
#define CLIENT_H

#include <QString>
#include <QLocalSocket>
#include <QDataStream>
#include <QTime>

class Client : public QObject
{
  Q_OBJECT
public:
  Client(QString serverName);
  ~Client();

  void sendToServer(QString message);
  void connectToServer();
  bool getStatus();
  void closeConnection();

signals:
  void statusChanged(bool);
  void error(QString error);
  void hasReadSome(QString msg);

private slots:
  void onReadyRead();
  void onError(QLocalSocket::LocalSocketError error);
  void onStateChanged(QLocalSocket::LocalSocketState state);

private:
  bool status_;
  QString serverName_;
  QLocalSocket* client_;
  enum {max_length = 58400};
  char receive_buf[max_length];
  unsigned int receive_len;
};

#endif // CLIENT_H
