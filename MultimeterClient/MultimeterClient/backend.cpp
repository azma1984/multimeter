#include "backend.h"

Backend::Backend(QObject *parent) : QObject(parent)
{
  client_ = new Client("/home/multimeter_socket");

  setStatus(client_->getStatus());

  connect(client_, &Client::hasReadSome, this, &Backend::receivedSomething);
  connect(client_, &Client::statusChanged, this, &Backend::setStatus);
  connect(client_, &Client::error , this, &Backend::gotError);
}

Backend::~Backend()
{
  delete client_;
}

bool Backend::getStatus()
{
  return client_->getStatus();
}

void Backend::setStatus(bool newStatus)
{
  if(newStatus)
  {
    emit statusChanged("CONNECTED");
  }
  else
  {
    emit statusChanged("DISCONNECTED");
  }
}

void Backend::receivedSomething(QString msg)
{
  emit someMessage(msg);
}

void Backend::gotError(QString error)
{
  emit someError(error);
}

void Backend::connectClicked()
{
  client_->connectToServer();
}

void Backend::sendClicked(QString msg)
{
  client_->sendToServer(msg);
}

void Backend::disconnectClicked()
{
  client_->closeConnection();
}
