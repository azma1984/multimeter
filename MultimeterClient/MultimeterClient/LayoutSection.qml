import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Rectangle {
    Layout.leftMargin: 15
    Layout.rightMargin: 15
    Layout.topMargin: 5
    Layout.bottomMargin: 5
    Layout.fillWidth: true
    color: "#F4F2F5"
    border.color: "gray"
    border.width: 1
}
