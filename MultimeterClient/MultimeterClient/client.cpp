#include "client.h"

Client::Client(QString serverName) : QObject()
{
  serverName_ = serverName;

  client_ = new QLocalSocket(this);

  connect(client_, SIGNAL(stateChanged(QLocalSocket::LocalSocketState)), SLOT(onStateChanged(QLocalSocket::LocalSocketState)));
  connect(client_, SIGNAL(readyRead()), SLOT(onReadyRead()));
  connect(client_, SIGNAL(error(QLocalSocket::LocalSocketError)),SLOT(onError(QLocalSocket::LocalSocketError)));
  connect(client_, SIGNAL(QLocalSocket::disconnected), this, SLOT(closeConnection));

  status_ = false;
}

Client::~Client()
{
  client_->abort();
}

void Client::onReadyRead()
{
  int bytes_transferred = client_->bytesAvailable();

  if(bytes_transferred>0)
  {
    QByteArray arr = client_->readAll();

    memcpy(&receive_buf[receive_len],arr.data(),bytes_transferred);

    receive_len += bytes_transferred;

    if(receive_buf[receive_len-1]=='\r')
    {
      receive_buf[receive_len-1]=0;
      receive_len = 0;
      emit hasReadSome(arr);
    }
  }
}

void Client::onError(QLocalSocket::LocalSocketError error)
{
  QString strError =  "Error: " + client_->errorString();

  emit this->error(strError);
}

void Client::sendToServer(QString message)
{
  message = message + "\r";

  int len = message.length();
  int bytes_transferred;
  int sum_bytes_transfered = 0;

  while(sum_bytes_transfered!=len)
  {
    bytes_transferred = client_->write(&message.toStdString()[sum_bytes_transfered]);

    if(bytes_transferred>0)
    {
      sum_bytes_transfered += bytes_transferred;
    }

  }
}

void Client::onStateChanged(QLocalSocket::LocalSocketState state)
{
  if(state==QLocalSocket::LocalSocketState::ConnectedState)
  {
    status_ = true;
  }
  else
  {
    status_ = false;
  }
  emit statusChanged(status_);
}

void Client::connectToServer()
{
  client_->setServerName(serverName_);
  client_->connectToServer();
  receive_len = 0;
}

void Client::closeConnection()
{
  client_->disconnectFromServer();
}

bool Client::getStatus()
{
  return status_;
}
