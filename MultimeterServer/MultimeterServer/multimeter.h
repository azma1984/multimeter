#ifndef MULTIMETER_H
#define MULTIMETER_H

#include "uds_server.h"
#include "commands.h"
#include "channels.h"

class Multimeter
{
private:
  static Commands commands;
  static Channels channels;
  static Command commandParser(char *buf);
  static std::string commandExecution(Command &command);
  static void callbackProcessingRequest(char *buf);
  UDSServer *server;

public:
  void readingArguments(int argc, char *argv[]);
  int run();
  Multimeter();
  ~Multimeter();
};

#endif // MULTIMETER_H
