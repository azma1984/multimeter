#ifndef CHANNEL_H
#define CHANNEL_H

#include <thread>
#include <mutex>
#include <time.h>
#include "range.h"
#include <string>
#include <random>

enum State
{
  error,
  idle,
  measure,
  busy
};


class Channel
{
private:

  std::mutex mtx;
  Range range;
  float value;
  State state;

  std::mutex start_measure_mutex;
  bool start_measure;

  std::thread spawn();
  std::thread measure_thread;
  void run();
public:
  void startMeasure();
  void stopMeasure();
  void setRange(const Range &range);
  State getState();
  float getResult();

  std::string getResultString();

  float generateDoubleRandom(const float &min, const float &max);

  float round(float x);

  Channel(const Channel& channel);

  Channel& operator = (const Channel &channel)
  {
    range = channel.range;
    value = channel.value;
    state = channel.state;
    return *this;
  }

  void clear();

  Channel();
  ~Channel();
};

#endif // CHANNEL_H
