#include "multimeter.h"

Commands Multimeter::commands;
Channels Multimeter::channels;

using namespace std;

Multimeter::Multimeter()
{
  Multimeter::commands.addCommand("start_measure",{"ok","falil"});
  Multimeter::commands.addCommand("set_range",{"ok","falil"});
  Multimeter::commands.addCommand("stop_measure",{"ok","falil"});
  Multimeter::commands.addCommand("get_status",{"ok","falil","error_state","idle_state","measure_state","busy_state"});
  Multimeter::commands.addCommand("get_result",{"ok","falil"});

  server = new UDSServer(Multimeter::callbackProcessingRequest);
}

Multimeter::~Multimeter()
{
  delete server;
}

void Multimeter::readingArguments(int argc, char *argv[])
{
  if(strcmp(argv[1], "-ts") == 0 || strcmp(argv[1], "--threads") == 0)
  {

  }
}


int Multimeter::run()
{
  return server->run();
}


void Multimeter::callbackProcessingRequest(char *buf)
{
  Command command = commandParser(buf);

  if(command.getName().empty()==false)
  {
    string result = commandExecution(command);

    result = result + "\r";

    memcpy((void*)buf,(void*)result.c_str(),sizeof(buf));
  }
  else
  {
    strcpy(buf,"fail\r");
  }
}

Command Multimeter::commandParser(char *buf)
{
  std::vector<string> pars;

  string cmd;
  string response;

  char *pch = strtok(buf," ");

  if(pch!=nullptr)
  {
    cmd = pch;
  }

  while (pch != nullptr)
  {
    pch = strtok(nullptr,", ");
    if(pch!=nullptr)
    {
      std::string par;
      par = pch;
      size_t pos = par.find('\r');
      if(pos!=string::npos)
      {
        par.resize(pos);
      }
      pars.push_back(par);
    }
  }


  Command command;
  bool ret = commands.fillParsCommand(cmd,pars,command);
  if(ret==false)
  {
    perror(string("Command "+cmd+"not found").c_str());
  }

  return command;
}



string Multimeter::commandExecution(Command &command)
{
  string result;

  unsigned int channel_number[NUMBER_CHANNLES];

  for(int i=0;i<NUMBER_CHANNLES;i++)
  {
    channel_number[i]=0;
  }

  bool success;

  success = command.getChannel(channel_number);


  if(success==false)
  {
    perror(string("Name channel "+command.getName()+" not found").c_str());
    return result;
  }


  string cmd = command.getName();


  if(cmd=="start_measure")
  {
    success = channels.startMeasure(channel_number);

    if(success==true)
    {
      result = command.getReturnResult(0);
    }
    else
    {
      result = command.getReturnResult(1);
    }
  }

  if(cmd=="set_range")
  {
    Range range[NUMBER_CHANNLES];

    for(int i=0;i<NUMBER_CHANNLES;i++)
    {
      range[i].min=0;
      range[i].max=0;

      if(channel_number[i]==true)
      {
        command.getRange(range,i);
      }
    }


    success = channels.setRange(channel_number,range);

    if(success==true)
    {
      result = command.getReturnResult(0);
    }
    else
    {
      result = command.getReturnResult(1);
    }

    result = result + ", range";
  }


  if(cmd=="stop_measure")
  {
    success = channels.stopMeasure(channel_number);

    if(success==true)
    {
      result = command.getReturnResult(0);
    }
    else
    {
      result = command.getReturnResult(1);
    }
  }


  if(cmd=="get_status")
  {
    State state;
    success = channels.getState(channel_number,state);

    if(success==true)
    {
      result = command.getReturnResult(0);
    }
    else
    {
      result = command.getReturnResult(1);
    }

    string string_state;
    switch(state)
    {
      case error:   string_state = "error_state";
                    break;
      case idle:    string_state = "idle_state";
                    break;
      case measure: string_state = "measure_state";
                    break;
      case busy:    string_state = "busy_state";
                    break;
      default:  string_state = "measure_state";

    }


    result = result + ", " + string_state;
  }


  if(cmd=="get_result")
  {
    success = command.getChannels(channel_number);

    if(success==false)
    {
      return command.getReturnResult(1);
    }


    string value[NUMBER_CHANNLES];
    success = channels.getResult(channel_number,value);
    result = command.getReturnResult(0);
    if(success==true)
    {
      for (int i=0;i<NUMBER_CHANNLES;i++)
      {
        if(channel_number[i]==true)
        {
          result = result + ", " + value[i];
        }
      }
    }
    else
    {
      result=command.getReturnResult(1);
    }

  }

  return result;
}
