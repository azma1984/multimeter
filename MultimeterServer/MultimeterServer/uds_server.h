#ifndef UDS_SERVER_H
#define UDS_SERVER_H

#include <signal.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <string.h>
#include <stdlib.h>
#include <sys/un.h>
#include <errno.h>
#include <sys/signal.h>
#include <wait.h>
#include <unistd.h>
#include <pthread.h>


#define PID_PATH "/home/multimeter_server.pid"
#define SOCK_PATH "/home/multimeter_socket"
#define LOG_PATH "/home/multimeter_server.log"


typedef void (*p_callbackProcessingRequest) (char *buf);

//Unix domain socket server
class UDSServer
{
private:
  int client_sockfd[1024];
  struct sockaddr_un remote;
  unsigned int t;
  int server_sockfd;
  pthread_t tid[1024];
  int cnt;

  void initSocket();

  void forkHandler(int);
  static void *handleConnection(void *arg);
  static void handleChild(int sig);

  static void stopServer(int sig);

  static p_callbackProcessingRequest callbackProcessingRequest;

public:

  int run();
  UDSServer(p_callbackProcessingRequest callbackProcessingRequest1 = nullptr);
};

#endif // UDS_SERVER_H
