#include "channel.h"

Channel::Channel()
{
  this->clear();
}

Channel::~Channel()
{

}

Channel::Channel(const Channel& channel)
{
  range = channel.range;
  value = channel.value;
  state = channel.state;
}

void Channel::clear()
{
  range.min=1;
  range.max=1000;
  value = 0;
  state = idle;
}

std::thread Channel::spawn()
{
  return std::thread( [this] { run(); } );
}

void Channel::startMeasure()
{
  start_measure=true;
  measure_thread = spawn();
}

void Channel::stopMeasure()
{
  start_measure_mutex.lock();
  start_measure=false;
  start_measure_mutex.unlock();

  if(measure_thread.joinable())
  {
    measure_thread.join();
  }

}

void Channel::setRange(const Range &range)
{
  std::lock_guard<std::mutex> lck (mtx);

  this->range = range;
}

State Channel::getState()
{
  std::lock_guard<std::mutex> lck (mtx);

  return state;
}

float Channel::getResult()
{
  std::lock_guard<std::mutex> lck (mtx);

  return value;
}

float Channel::round(float x)
{
 return x < 0 ? ceil (x-0.5) : floor (x+0.5);
}

std::string Channel::getResultString()
{
  std::lock_guard<std::mutex> lck (mtx);

  char buf[256];
  int n;

  if((range.min>=0.0000001)&&(range.min<0.001))
  {
    n = sprintf(buf,"%0.7f",value);
  }

  if((range.min>=0.001)&&(range.min<1.0))
  {
    n = sprintf(buf,"%1.3f",value);
  }

  if(range.min>=1.0)
  {
    n = sprintf(buf,"%f",round(value));
  }

  std::string str;
  str = buf;
  str.erase(str.find_last_not_of("0")+1);
  if(str.back()=='.')
  {
    str.erase(str.length()-1);
  }


  if((state==error)||(state==busy))
  {
    str = "fail";
  }

  return str;
}

void Channel::run()
{
  std::thread::id id = std::this_thread::get_id();

  while(1)
  {
    {
      std::lock_guard<std::mutex> lck (mtx);
      state = measure;

      if(value<range.max/5)
      {
        state = error;
      }

      if((value>range.max/5)&&(value<range.max/4))
      {
        state = busy;
      }


      value = generateDoubleRandom(range.min,range.max);

      printf("id = %d, value = %f, state = %d\n",id,value,state);
      fflush(stdout);
    }

    std::this_thread::sleep_for(std::chrono::seconds(1));

    start_measure_mutex.lock();
    if(start_measure==false)
    {
      start_measure_mutex.unlock();
      break;
    }
    start_measure_mutex.unlock();
  }
}

//thread safe random number generator from min to max inclusive
float Channel::generateDoubleRandom(const float &min, const float &max)
{
  float generatedValue;

  std::uniform_real_distribution<> distribution(min, std::nextafter( max, std::numeric_limits<float>::max() ));

  static thread_local std::mt19937* generator = nullptr;

  if(!generator)
  {
    generator = new std::mt19937(clock() + std::hash<std::thread::id>()(std::this_thread::get_id()));
  }

  generatedValue = distribution(*generator);

  if(generatedValue < min)
  {
    generatedValue = min;
  }

  if(generatedValue > max)
  {
    generatedValue = max;
  }

  return generatedValue;
}
