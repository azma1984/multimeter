#ifndef COMMAND_H
#define COMMAND_H

#include <string>
#include <vector>
#include <list>
#include <mutex>
#include "range.h"

class Command
{
private:
  std::mutex mtx;
  std::string name;
  std::vector<std::string> parameters;
  std::vector<std::string> result;

  Range ranges[5];
public:
  void setName(const std::string &name);
  std::string& getName();
  void fillParameters(const std::vector<std::string> &parameters);
  void setReturnResult(const std::vector<std::string> &result);
  std::string getReturnResult(unsigned int i);
  bool getChannel(unsigned int *channel);
  bool getChannels(unsigned int *channel);
  void getRange(Range *range,unsigned int number_channel);

  Command(const Command& command);

  Command& operator = (const Command &command)
  {
    this->name = command.name;
    this->parameters = command.parameters;
    this->result = command.result;

    return *this;
  }

  void clear();

  Command();
  ~Command();
};

#endif // COMMAND_H
