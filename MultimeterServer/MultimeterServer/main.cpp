#include <iostream>

#include "multimeter.h"

using namespace std;

int main(int argc, char *argv[])
{
  Multimeter multimeter;

  if(argc > 1)
  {
    multimeter.readingArguments(argc, argv);
  }

  int ret = multimeter.run();

  return ret;
}
