TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

LIBS +=-pthread

SOURCES += main.cpp \
    uds_server.cpp \
    channels.cpp \
    channel.cpp \
    command.cpp \
    commands.cpp \
    multimeter.cpp

HEADERS += \
    uds_server.h \
    channels.h \
    channel.h \
    command.h \
    commands.h \
    range.h \
    multimeter.h
