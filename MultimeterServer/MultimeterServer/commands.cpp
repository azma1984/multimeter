#include "commands.h"

Commands::Commands()
{

}

void Commands::addCommand(const std::string &name, const std::vector<std::string> &result)
{
  Command command;
  command.setName(name);
  command.setReturnResult(result);
  list.push_back(command);
}

bool Commands::fillParsCommand(const std::string &name, const std::vector<std::string> &pars, Command &command)
{
  std::lock_guard<std::mutex> lck (mtx);
  bool ret=false;


  for (std::list<Command>::iterator it=list.begin(); it != list.end(); ++it)
  {
    Command &command1 = *it;

    if(command1.getName()==name)
    {
      ret = true;

      command = command1;
      command.fillParameters(pars);
      break;
    }
  }

  return ret;
}
