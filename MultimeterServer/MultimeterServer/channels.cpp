#include "channels.h"

using namespace std;

Channels::Channels()
{
  list.resize(NUMBER_CHANNLES);
}

bool Channels::setRange(unsigned int *channel_number,Range *range)
{
  std::lock_guard<std::mutex> lck (mtx);
  unsigned int i=0;

  for (std::list<Channel>::iterator it=list.begin(); it != list.end(); ++it)
  {
    Channel &channel = *it;

    if(channel_number[i]!=0)
    {
      channel.setRange(range[i]);
    }

    i++;
  }


  return true;
}

bool Channels::startMeasure(unsigned int *channel_number)
{
  std::lock_guard<std::mutex> lck (mtx);
  unsigned int i=0;

  for (std::list<Channel>::iterator it=list.begin(); it != list.end(); ++it)
  {
    Channel &channel = *it;

    if(channel_number[i]!=0)
    {
      channel.startMeasure();
    }

    i++;
  }

  return true;
}

bool Channels::stopMeasure(unsigned int *channel_number)
{
  std::lock_guard<std::mutex> lck (mtx);
  unsigned int i=0;

  for (std::list<Channel>::iterator it=list.begin(); it != list.end(); ++it)
  {
    Channel &channel = *it;

    if(channel_number[i]!=0)
    {
      channel.stopMeasure();
    }

    i++;
  }

  return true;
}


bool Channels::getState(unsigned int *channel_number,State &state)
{
  std::lock_guard<std::mutex> lck (mtx);
  unsigned int i=0;

  for (std::list<Channel>::iterator it=list.begin(); it != list.end(); ++it)
  {
    Channel &channel = *it;

    if(channel_number[i]!=0)
    {
      state = channel.getState();
    }

    i++;
  }

  return true;
}


bool Channels::getResult(unsigned int *channel_number,string *result)
{
  std::lock_guard<std::mutex> lck (mtx);

  unsigned int i=0;

  for (std::list<Channel>::iterator it=list.begin(); it != list.end(); ++it)
  {
    Channel &channel = *it;

    if(channel_number[i]!=0)
    {
      if((channel.getState()==error)||(channel.getState()==busy))
      {
        return false;
      }
      result[i] = channel.getResultString();
    }

    i++;
  }

  return true;
}
