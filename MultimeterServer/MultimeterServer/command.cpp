#include "command.h"

using namespace std;

Command::Command()
{
  this->clear();
}


Command::Command(const Command& command)
{
  this->name = command.name;
  this->parameters = command.parameters;
  this->result = command.result;
}

Command::~Command()
{

}

void Command::clear()
{
  this->name.clear();
  this->parameters.clear();
  this->result.clear();

  this->ranges[0] = {0.0000001,0.001};
  this->ranges[1] = {0.001,1};
  this->ranges[2] = {1,1000};
  this->ranges[3] = {1000,1000000};

}


void Command::setName(const string &name)
{
  this->name = name;
}

std::string& Command::getName()
{
    std::lock_guard<std::mutex> lck (mtx);
  return name;
}

void Command::fillParameters(const std::vector<std::string> &parameters)
{
  this->parameters = parameters;
}

void Command::setReturnResult(const vector<string> &result)
{
  this->result = result;
}


string Command::getReturnResult(unsigned int i)
{
  std::lock_guard<std::mutex> lck (mtx);
  return this->result.at(i);
}

bool Command::getChannel(unsigned int *channel)
{
  std::lock_guard<std::mutex> lck (mtx);
  bool ret=false;
  string str_channel = parameters.at(0);
  unsigned int i = std::stoul(str_channel.substr(7));

  if(i<NUMBER_CHANNLES)
  {
    channel[i] = 1;
    ret=true;
  }

  return ret;
}

bool Command::getChannels(unsigned int *channel)
{
  std::lock_guard<std::mutex> lck (mtx);
  bool ret=false;

  for (std::vector<string>::iterator it = parameters.begin() ; it != parameters.end(); ++it)
  {
    string str_channel = (*it);
    unsigned int i = std::stoul(str_channel.substr(7));

    if(i<NUMBER_CHANNLES)
    {
      channel[i] = 1;
      ret=true;
    }
  }

  return ret;
}


void Command::getRange(Range *range,unsigned int number_channel)
{
  std::lock_guard<std::mutex> lck (mtx);
  string str_range  = parameters.at(1);
  unsigned int i = std::stoul(str_range.substr(5));

  range[number_channel] = ranges[i];
}

