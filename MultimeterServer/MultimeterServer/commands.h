#ifndef COMMANDS_H
#define COMMANDS_H

#include "command.h"
#include <vector>
#include <list>
#include <mutex>

class Commands
{
private:
  std::mutex mtx;
  std::list<Command> list;
public:
  void addCommand(const std::string &name, const std::vector<std::string> &result);
  bool fillParsCommand(const std::string &name, const std::vector<std::string> &pars,Command &command);
  Commands();
};

#endif // COMMANDS_H
