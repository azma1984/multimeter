#include "uds_server.h"

using namespace std;

p_callbackProcessingRequest UDSServer::callbackProcessingRequest;

UDSServer::UDSServer(p_callbackProcessingRequest callbackProcessingRequest1)
{
  callbackProcessingRequest = callbackProcessingRequest1;
}

int UDSServer::run()
{
  signal(SIGCHLD, &handleChild);
  signal(SIGTERM, &stopServer);

  cnt=0;
  initSocket();

  if(listen(server_sockfd, 1025) == -1)
  {
    perror("listen");
    exit(1);
  }

  printf("Listening...\n");
  fflush(stdout);

  for(;;)
  {
    printf("Waiting for a connection\n");
    fflush(stdout);
    t = sizeof(remote);
    if((client_sockfd[cnt] = accept(server_sockfd, (struct sockaddr *)&remote, &t)) == -1)
    {
      perror("accept");
      exit(1);
    }

    pthread_create(&tid[cnt],nullptr,handleConnection,(void*)&client_sockfd[cnt]);

    printf("Accepted connection\n");
    fflush(stdout);

    cnt++;

    if(cnt>1024)
    {
      cnt = 0;

      printf("cnt > 1024 \n");
      fflush(stdout);

      break;
    }

  }

  close(server_sockfd);

  return 0;
}



void *UDSServer::handleConnection(void *arg)
{
  char buf[2048] = {0};


  int client_sockfd = *((int*)arg);

  printf("Handling connection\n");
  fflush(stdout);

  int received_bytes = 0;

  while(received_bytes = recv(client_sockfd, &buf, 2048, 0), received_bytes>0)
  {
    if(buf[received_bytes-1]=='\r')
    {
      callbackProcessingRequest(buf);

      send(client_sockfd, &buf, strlen(buf), 0);
    }
  }

  close(client_sockfd);

  printf("Done handling\n");
  fflush(stdout);

  return nullptr;
 }

void UDSServer::handleChild(int sig)
{
  int status;

  printf("Killing child\n");
  fflush(stdout);

  wait(&status);
}




void UDSServer::stopServer(int sig)
{
  unlink(PID_PATH);
  unlink(SOCK_PATH);
  kill(0, SIGKILL);
  exit(0);
}

void UDSServer::initSocket()
{
  struct sockaddr_un local;
  int len;

  if((server_sockfd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1)
  {
    perror("Error creating server socket");
    exit(1);
  }

  local.sun_family = AF_UNIX;
  strcpy(local.sun_path, SOCK_PATH);
  unlink(local.sun_path);
  len = strlen(local.sun_path) + sizeof(local.sun_family);
  if(bind(server_sockfd, (struct sockaddr *)&local, len) == -1)
  {
    perror("binding");
    exit(1);
  }
}
