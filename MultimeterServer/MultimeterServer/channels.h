#ifndef CHANNELS_H
#define CHANNELS_H

#include <list>
#include <channel.h>
#include <mutex>

class Channels
{
private:
  std::mutex mtx;
  std::list<Channel> list;
public:
  bool setRange(unsigned int *channel_number,Range *range);
  bool startMeasure(unsigned int *channel_number);
  bool stopMeasure(unsigned int *channel_number);
  bool getState(unsigned int *channel_number,State &state);
  bool getResult(unsigned int *channel_number,std::string *result);
  Channels();
};

#endif // CHANNELS_H
